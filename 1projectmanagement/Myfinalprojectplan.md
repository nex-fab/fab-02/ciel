## Initial idea

The plan of my initial idea about my final project

I want to make a Panda by 3D printing. 

Here some photos from website that pandas living.

![](https://gitlab.com/cielpic/picbed/uploads/532ebce7c2d03f157f183a75e13b9e5c/20200730164436.png)

![](https://gitlab.com/cielpic/picbed/uploads/9269b35b199c6c7418d8ad9b0825c0e5/20200730164453.png)

This panda can be placed in the desk as a toy.

![](https://gitlab.com/cielpic/picbed/uploads/94aecfafff041c5d73a8306926230895/20200730164527.png)

## Updated plan

Maybe this is a panda with lighting eys or movable ears. Or it can be regarded as Bionic Panda.

![](https://gitlab.com/cielpic/picbed/uploads/d4efb78933da61f222bdc56e52e9278b/20200803100941.png)

![](https://gitlab.com/cielpic/picbed/uploads/9d388e26fdf864fefebba41ab5286b60/20200803100902.png)