# Summary

* [1.Project myself](https://www.nexmaker.com/doc/1projectmanage/Assessment1project-manage.html) 

    * [Introduce Myself](1projectmanagement/Introducemyself.md)
    * ~~[My Final project plan](1projectmanagement/Myfinalprojectplan.md)~~

    * [Jenny&Ciel Final project](1projectmanagement/Jenny&Ciel.md)

* [2.CAD](https://www.nexmaker.com/doc/2cad/cad.html)
    * [First work -Cup 07/30/2020](2CAD/Firstworkcup.md)

* [3.3DPRINTING](https://www.nexmaker.com/doc/3_3dprinter/assignment.html)
    * [3D Printing CUP 08/04/2020](33DPRINTING/33DPrinting-cup.md)

* [4.ArduinoApplication](https://www.nexmaker.com/doc/1projectmanage/Tool1Project-manage.html)
    * [LED lights 08/18/2020](4ArduinoApplication/Thefirstclass-LEDlights.md)

    * [Arduino-感光灯 08/20/2020](4ArduinoApplication/Arduino-ActinicLight.md) 

* [5.Laser cutting](https://www.nexmaker.com/doc/6laser_cutter/basic.html)
    * [AutoCAD 08/25/2020](5.Lasercutting/AutoCAD.md)
    
    