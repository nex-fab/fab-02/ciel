# **Solid works** 

**SOLIDWORKS** is used to _develop mechatronics systems from beginning to end_ . At the initial stage, the software is used for planning, visual ideation, modeling, feasibility assessment, prototyping, and project management. The software is then used for design and building of mechanical, electrical, and software elements.

My teammate is **Jenny**. Our plan is to make a cup with a cap. _Like this_:

![](https://gitlab.com/cielpic/picbed/uploads/3c14256d0ed900e70a3bdd7e5dbdf509/20200803103705.png)

The proessing of our team work.

## 1. **选择一个基准面（向上基准面）**
Choose Datum plane (upward datum plane)

![](https://gitlab.com/cielpic/picbed/uploads/090019986ee19367f510a6a58103b4cb/6301b4652d4ef6b1de016437d163752.png)

## 2. **草图的中心位置画一个圆**
Draw a circle in draft

![](https://gitlab.com/cielpic/picbed/uploads/192352e4046f1714e686fff11b04b42b/0f05180223b0b2e6e45d82c92b60eef.png)

## 3. **在特征里面选择拉伸命令，拉伸出高度**
Select the extrude command in the feature to extrude the height

![](https://gitlab.com/cielpic/picbed/uploads/57258f041da5deb3003acca461e4f4d4/fab270f744e8aa5c9037b823de6b0e1.png)

## 4. **在特征里面选择拔模，设置角度**
Select the draft in the feature and set the angle

![](https://gitlab.com/cielpic/picbed/uploads/2e7d9b50d468f47d2f6575801b1cd1cc/767cb4af083a73a1b1a193bd448dcee.png)

![](https://gitlab.com/cielpic/picbed/uploads/e512fd71da4e8991bd593264a89c94e4/0edd27eeabae25ea80c2349311bbbb4.png)

## 5. **选中一个面，在特征里面选择抽壳命令，掏空内部**
Select a face, select the shell command in the feature, and hollow out the interior

![](https://gitlab.com/cielpic/picbed/uploads/f62e9a76f0c3c909c9b7aea3b55f82a5/cd8a009f3d3023ea0c2cf75da55a3aa.png)

![](https://gitlab.com/cielpic/picbed/uploads/2dd693fc7779fef69c3de0faddbe023d/3d2b20e7d40f4470fb32d919f77e25c.png)

## 6. **方法一 "保存之后在新建组装体里面新建，创建盖子"- 转换实体，等距实体的操作命令** 

![](https://gitlab.com/cielpic/picbed/uploads/81e2b86327527c53f9f03f7128af705d/e33c4cc4d833207e43c4115135b40cd.png)

![](https://gitlab.com/cielpic/picbed/uploads/7a1abb8ff8f27938d387e6302e024127/eca56f5b370691a105a5375b7e8c585.png)

![](https://gitlab.com/cielpic/picbed/uploads/012eeb8aaf67b35a4548d8a6c6548c3a/f95a3744ed61220d4e638019b470a8e.png)

![](https://gitlab.com/cielpic/picbed/uploads/e9f9f36477c51102cda1ee91d641d4de/2f3991907f3c0ebbae858152c87f251.png)

![](https://gitlab.com/cielpic/picbed/uploads/b17dc33b183365935b8a12d86ca9cd89/1a6e3397466d1afa6e1fe0a4602e997.png)


![](https://gitlab.com/cielpic/picbed/uploads/578f3846ba792bee1313f20113a7e403/9e30cd76c3317abe5872f8e61b5df04.png)

![](https://gitlab.com/cielpic/picbed/uploads/d299fc6bbdb049692bbbe31efad56132/da1e433d3c165680959bd61bcbc0e6f.png)

![](https://gitlab.com/cielpic/picbed/uploads/64d2ac4e68b4f896af6d8270913bfb29/1001e0c38a438587d156f80a76e1f7f.png)

## 7. **方法二 用旋转命令创建盖子(02)**

https://drive.google.com/drive/folders/13JrjvTj6ZpqTnCxa42t5OUg4NEqsbNBz

## 8. **文件存储**
https://drive.google.com/drive/folders/11tem1oM93GF78-MYD0-Wly-c_m-G_X86
